#include <iostream>

using namespace std;

template <class T> 
class List{
	private:
		struct Person{
			T one;
			Person *next;
		};
		Person *head;
		int number;	//number of people
		void copyList(const List<T> &x);
		void removeAll();
		void removePerson(const T &x);
	public:
		List();
		List(const List<T> &x);
		~List();
		void print();
		T findPerson(int id);	// <T> requires getID method
		List& operator= (const List<T> &x);
		List operator+ (const List<T> &x) const;
		List& operator+= (const List<T> &x);
		List& operator+= (const T &x);
		List& operator++();
		List operator-= (const List<T> &x);
		List& operator-= (const T &x);
};

template <class T>
List<T>::List() : head(NULL), number(0){}

template <class T>
List<T>::List(const List<T> &x) : head(NULL), number(0){
	copyList(x);
}

template <class T>
List<T>::~List(){
	removeAll();
}

template <class T>
T List<T>::findPerson(int id){
	Person* target = head;
	while(!(((target->one)->getID) == id) || target){
		target = target->next;
	}
	return target->one;
}

template <class T>
List<T>& List<T>::operator=(const List<T> &x){
	if(&x == this)
		return *this;
	removeAll();
	copyList(x);
	return *this;
}

template <class T>
List<T> List<T>::operator+(const List<T> &x) const{
	List<T> result;
	result.copyList(*this);
	result.copyList(x);
	return result;
}

template <class T>
List<T>& List<T>:: operator+= (const List<T> &x){
	List<T> result(*this);
	result.copyList(x);
	(*this) =  result;
	return *this;
}

template <class T>
List<T>& List<T>::operator+= (const T &x){
	Person* newest = new Person;
	newest->one = x;
	newest->next = head;
	head = newest;
	number++;
	return *this;
}

template <class T>
List<T>& List<T>::operator++ (){
	Person* newest = new Person;
	(newest->one).setData();
	newest->next = head;
	head = newest;
	number++;
	return *this;
}

template <class T>
List<T> List<T>::operator-=(const List<T> &x){
	Person* sb = x.head;
	Person* sm = sb->next;
	while(sm){
		removePerson(sb->one);
		sb = sm;
		sm = sm->next;
	}
	return *this;
}

template <class T>
List<T>& List<T>::operator-= (const T &x){
	removePerson(x);
	return *this;
}

template <class T>
void List<T>::print(){
	if(!head)
		return;
	Person* ptr;
	ptr = head;
	while(ptr){
		cout << (ptr->one) << endl;
		ptr = ptr->next;
	}
}

template <class T>		
void List<T>::copyList(const List<T> &x){
	Person* ptr = x.head;
	Person* etr = head;
	if(etr != NULL){
		while(etr->next)
			etr = etr->next;
	}
	while(ptr){
		Person* xtr = new Person;
		xtr->one = ptr->one;
		if(!head){
			etr = xtr;
			head = etr;
			ptr = ptr->next;
			number++;
			continue;
		}
		else
			etr->next = xtr;
		etr = etr->next;
		ptr = ptr->next;
		number++;
	}
}

template <class T>
void List<T>::removeAll(){
	Person* ptr = head;
	while(head){
		ptr = head;
		delete ptr;
		head = ptr->next;
	}
	number = 0;
}

template <class T>
void List<T>::removePerson(const T &x){
	Person* sm = head;
	if((sm->one).getID() == x.getID()){
		head = head->next;
		delete sm;
		return;
	}
	Person* so;
	while(sm->next){
		so = sm;
		sm = sm->next;
		if((sm->one).getID() == x.getID()){
			Person* sb;
			sb = sm->next;
			so->next = sb;
			delete sm;
			return;
		}
	}
	if(((sm->next)->one).getID() == x.getID()){
		so = sm->next;
		delete so;
		sm->next = NULL;
		return;
	}
}
