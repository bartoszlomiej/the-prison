#include "human.h"

using namespace std;

Human::Human(){}

Human::Human(int ida, string namea, string surnamea, int privilegesa){
	id = ida;
	name = namea;
	surname = surnamea;
	privileges = privilegesa;
}

void Human::setPrivileges(){
	cout << "Are you sure you want to change the privileges? (y/n)" << endl;
	char c;
	//scanf("%c", &c);
	cin >> c;
	if (c == 'y')
	{
		int x;
		cout << "set the value of privileges: " << endl;
		cin >> x;
		privileges = x;
	}
	else if(c == 'n')
	{
		cout << "Option abandoned" << endl;
		return;
	}
	else
	{
		cout << "Wrong data. Option abandoned" << endl;
		return;
	}
}

bool Human::checkPrivileges(int x){
	if(x == privileges)
		return 1;
	else
		return 0;
}

int Human::getID() const{
	return id;
}

void Human::setData(){
	cout << "Give new person's id: ";
	cin >> id;
	cout << "Give person name: ";
	cin >> name;
	cout << "Give surname: ";
	cin >> surname;
	cout << "Give privileges: ";
	cin >> privileges;
}

void Human::print(){
	cout << endl <<"Person: " << id << ", name: " << name << ", surname: " << surname << endl;
	cout << "Privilages: " << privileges << endl;
}
