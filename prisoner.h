#include <iostream>
#include <string>
#include "human.h"

using namespace std;

#ifndef PRISONER
#define PRISONER

class Prisoner : public Human{
	private:
			string notes;
			int dangerLvl;
	public:
		Prisoner() : Human(){}
		Prisoner(int id, string name, string surname, string notes,int dangerLvl, int privileges);
		Prisoner(int id, string name, string surname, string notes,int dangerLvl);
		Prisoner(int id, string name, string surname, string notes);
		Prisoner(int id, string name, string surname);
		~Prisoner();
		void setDangerLvl(int);	
		void addNote();
		friend ostream& operator<< (ostream& os, Prisoner &x);
};

#endif
