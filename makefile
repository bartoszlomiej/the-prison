CC = g++
CFLAGS = -I. -pedantic -Wall -Wextra -std=c++1z

DEPS = prisoner.h ulist.h worker.h human.h room.h
OBJ = main.o prisoner.o worker.o human.o room.o

%.o: %.cpp $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

Prison: $(OBJ)
	$(CC) -o $@ $^ $(FLAGS)

.PHONY: clean

clean: 
	-rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~

