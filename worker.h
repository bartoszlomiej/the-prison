#include <iostream>
#include "human.h"

using namespace std;

#ifndef WORKER
#define WORKER

class Worker : public Human{
	private:
			string role;
			char currentOrder;
	public:
		Worker(int, string, string, string, char, int);
		Worker(int, string, string, string, char);
		Worker(int, string, string);
		Worker();
		~Worker();
		void setRole(string s);
		void setOrder(char c);
		friend ostream& operator<< (ostream& os, Worker &x);
};

#endif
