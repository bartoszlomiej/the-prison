#include <iostream>
#include <cstdlib>
#include "room.h"

using namespace std;

int main(){
cout << "There will be breathly presented the most important features of the project 'The Prison'" << endl;
cout << "If you would like to start type any letter and press enter: " << endl;
string s;
cin>> s;
system("clear");	
//===================Part 1. Presenting the class Worker=============================================
cout << "At the very beginning there will be tested the class  Worker, which inharited most features from the class Human" << endl;
Worker Roman(1, "Roman", "Moravsky", "Guard chief", 'a', 20);
Worker John(2, "John", "Smith", "Guard", 'b', 10);
Worker Johnny(3, "Johnny", "Tipper", "Guard", 'b', 8);
Worker Kate(4, "Kate", "Middletone", "Kitchen service", 'c', 4);
Worker Tom(6, "Tom", "Thanks", "Kitchen service", 'c', 4);
Worker Adam(5, "Adam", "Smith", "Cleaning service", 'p', 2);

cout << "There were just created several objects, with the usage of different constructors, of type Worker." << endl;
cout << "If you want to print the press 'y': ";
char c;
cin >> c;
if(c == 'y'){
	cout << Roman;
	cout << John;
	cout << Johnny;
	cout << Kate;
	cout << Tom;
	cout << Adam;
}
cout << "Now you will be able to add a new worker. If you want to do this, press 'y': ";
cin >> c;
if(c == 'y'){
	Worker Vacancy;
	Vacancy.setData();
	Vacancy.setOrder('k');
	Vacancy.setRole("Newby");
	cout << Vacancy << endl;
}
cout << "to continue type any letter: ";
cin >> s;	
system("clear");
//===================Part 2. Presenting the funcitonality of the clas Prisoner=======================
cout << "class 'Prisoner' is going to be tested in the same way as previous one." << endl;
Prisoner Bob(6, "Bob", "Ford", "morderer", 12);
Prisoner Cris(7, "Cris", "Harris", "recidivist");
Prisoner Anne(9, "Anne", "Pinapplebaum", "propagandist", 3);
Prisoner Andrew(10, "Andrew", "Softina", "innocent", 13);
Prisoner Hans(8, "Hans", "Timmermans");
cout << "if you want to show all the prisoners press 'y': ";
cin >> c;
if(c == 'y'){
	cout << Bob;
	cout << Cris;
	cout << Anne;
	cout << Andrew;
	cout << Hans;
}
cout << "The prisoner Hans doesn't have the danger level and note. If you want to add it press 'y': ";
cin >> c;
if(c == 'y'){
	Hans.setDangerLvl(7);
	Hans.addNote();
	cout << Hans << endl;
}

//===================Part 3. Presenting the functionality of workersList=============================
cout << "If you want to continue, type any letter: ";
cin >> s;
system("clear");
cout << "The next part that must be tested is the universal class List" << endl;
cout << "It was build on the templates, thus it is ideal for both workersList and prisonersList" << endl;
cout << "Firstly, there will be initialized 3 list of workers:" << endl;
cout << "To the lists it is easy to add new worker, join them, add them, and even substract" << endl;
List<Worker> workerList1;	
workerList1 += Roman;
workerList1 += John;
workerList1 += Johnny;
List<Worker> workerList2(workerList1);	
workerList2 += Kate;
List<Worker> workerList3;
workerList3 += Tom; 
cout << "If you want see the example, press 'y': "; 
cin >> c;		
system("clear");
if(c == 'y'){
	cout << "WorkerList3: " << endl;
	workerList3.print();
	cout << "Adding new member to workerList3: " << endl;
 	workerList3 += Adam;
	workerList3.print();
	cout << "WorkerList2: " << endl;
	workerList2.print();
	cout << "WorkerList3 + WorkerList 2: " << endl;
	List<Worker> workerList4;
	workerList4 = workerList3 + workerList2;
	workerList4.print();
}

//================Part 4. Creating prisonersList=====================================================
cout << "If you want to continue, type any letter: " << endl;
cin >> s;
system("clear");
cout << "The same as previouesly, there were created few lists of prisoners" << endl;
cout << "which have the same functionality as the workerslists." << endl;
List<Prisoner> prisonersList1;
prisonersList1 += Bob;
prisonersList1 += Andrew;
List<Prisoner> prisonersList2;
prisonersList2 += Cris;
prisonersList2 += Bob;
prisonersList2 += Anne;
cout << "If you want to continue, type any letter: " << endl;
cin >> s;
system("clear");
cout << "If you want to show exemplary operation on those lists, press 'y' : ";
cin >> c;
if(c == 'y'){
	cout << "The prisonersList1: " << endl;
	prisonersList1.print();
	cout << "The prisonersList2: " << endl;
	prisonersList2.print();
}
//=================Part 5. Creating a room===========================================================
cout << "If you want to continue, type any letter: " << endl;
cin >> s;
system("clear");
cout << "Now, there will be created 2 rooms. The dinner room:" << endl;
Room dinnerRoom(2, 1, 10, 10, 20, workerList1, prisonersList1);
cout << dinnerRoom << endl;
cout << "And the socialRoom: " << endl;
Room socialRoom(1, 1, 5, 5, 15, workerList2, prisonersList2);
//=================Part 4. Other features============================================================
cout << socialRoom << endl;
cout << "One can add/substract the whole list of workers/prisoners to the room: " << endl;
dinnerRoom += workerList3;
cout << dinnerRoom << endl;
cout << "As well as, one can add just single person: " << endl;
dinnerRoom += Tom;
cout << dinnerRoom << endl;
cout << "The most important features of this project were presented." << endl;
cout << "In order to invastigate more, there is a need to read the code." << endl;
cout << "Thank you for your attention." << endl;
}
