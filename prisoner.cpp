#include "prisoner.h"
#include <iostream>

using namespace std;

Prisoner::Prisoner(int id, string name, string surname, string notesa, int dangerLvla, int privileges) : Human(id, name, surname, privileges){
	cout << "New Prisoner" << endl;
	notes = notesa;
	dangerLvl = dangerLvla;
}

Prisoner::Prisoner(int id, string name, string surname, string notesa, int dangerLvla) : Human(id, name, surname, 0){
	cout << "New Prisoner" << endl;
	notes = notesa;
	dangerLvl = dangerLvla;
}

Prisoner::Prisoner(int id, string name, string surname, string notesa) : Human(id, name, surname, 0){
	notes = notesa;
	dangerLvl = 1;
	cout << "New Prisoner" << endl;
}

Prisoner::Prisoner(int id, string name, string surname) : Human(id, name, surname, 0){
	notes = "First note\n";
	privileges = 0;
	dangerLvl = 0;
	cout << "New Prisoner" << endl;
}

Prisoner::~Prisoner(){
	cout << "Prisoner of id: " << id << " removed" << endl;
}

void Prisoner::addNote(){
	string newNote;
	cout << "Add note: ";
	cin >> newNote;
	notes += "\n" + newNote;
}

void Prisoner::setDangerLvl(int x){
	cout << "Are you sure you want to change Danger Level? (y/n)" << endl;
	char c;
	cin >> c;
	if (c == 'y')
	{
		cout << "Give a reason for change of danger lever" << endl;
		this->addNote();
		dangerLvl = x;
	}
	else if(c == 'n')
	{
		cout << "Option abandoned" << endl;
		return;
	}
	else
	{
		cout << "Wrong data. Option abandoned" << endl;
		return;
	}
}

ostream& operator<< (ostream& os, Prisoner &x){
	x.print();
	cout << "Denger level: " << x.dangerLvl << endl;
	cout << "Notes: " << x.notes << endl;
	return os;
}
