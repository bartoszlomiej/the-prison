#include "room.h"

using namespace std;

Room::Room(){
	roomID = 0;
	privilegesNeeded = 1;
	numberOfWorkers = 5;
	numberOfPrisoners = 5;
	maximalDangerLvl = 8;
	List<Worker> wlist;
	List<Prisoner> plist;
}

Room::Room(int id) : roomID(id){
	privilegesNeeded = 1;
	numberOfWorkers = 5;
	numberOfPrisoners = 5;
	maximalDangerLvl = 8;
	List<Worker> wlist;
	List<Prisoner> plist;
}

Room::Room(int id, int privileges) : roomID(id), privilegesNeeded(privileges) {
	numberOfWorkers = 5;
	numberOfPrisoners = 5;
	maximalDangerLvl = 8;
	List<Worker> wlist;
	List<Prisoner> plist;
}

Room::Room(int id, int privileges, int numberOfWorkers, int numberOfPrisoners, int maximalDangerLvl) : roomID(id), privilegesNeeded(privileges), numberOfWorkers(numberOfWorkers), numberOfPrisoners(numberOfPrisoners), maximalDangerLvl(maximalDangerLvl), wlist(){
	List<Worker> wlist;
	List<Prisoner> plist;
}

Room::Room(int id, int privileges, int numberOfWorkers, int numberOfPrisoners, int maximalDangerLvl, const List<Worker> &w) : roomID(id), privilegesNeeded(privileges), numberOfWorkers(numberOfWorkers), numberOfPrisoners(numberOfPrisoners), maximalDangerLvl(maximalDangerLvl), wlist(w){
	List<Prisoner> plist;
}


Room::Room(int id, int privileges, int numberOfWorkers, int numberOfPrisoners, int maximalDangerLvl, const List<Prisoner> &p) : roomID(id), privilegesNeeded(privileges), numberOfWorkers(numberOfWorkers), numberOfPrisoners(numberOfPrisoners), maximalDangerLvl(maximalDangerLvl), plist(p){
	List<Worker> wlist;
}

Room::Room(int id, int privileges, int numberOfWorkers, int numberOfPrisoners, int maximalDangerLvl, const List<Worker> &w, const List<Prisoner> &p) : roomID(id), privilegesNeeded(privileges), numberOfWorkers(numberOfWorkers), numberOfPrisoners(numberOfPrisoners), maximalDangerLvl(maximalDangerLvl), wlist(w), plist(p){}

Room::~Room(){
	cout << "Room: " << roomID << " removed." << endl;
}

Room::Room(const Room &x){
	roomID = x.roomID;
	privilegesNeeded = x.privilegesNeeded ;
	numberOfWorkers = x.numberOfWorkers; 
	numberOfPrisoners = x.numberOfPrisoners;
	maximalDangerLvl = x.maximalDangerLvl;
	wlist = x.wlist;
	plist = x.plist;
}

Room& Room::operator= (const Room & x){
	roomID = x.roomID;
	privilegesNeeded = x.privilegesNeeded ;
	numberOfWorkers = x.numberOfWorkers; 
	numberOfPrisoners = x.numberOfPrisoners;
	maximalDangerLvl = x.maximalDangerLvl;
	return *this;
}

Room& Room::operator+= (const Worker & x){
	wlist += x;
	return *this;
}

Room& Room::operator+= (const Prisoner & x){
	plist += x;
	return *this;
}

Room& Room::operator+= (const List<Worker> & x){
	wlist += x;
	return *this;
}

Room& Room::operator+= (const List<Prisoner> & x){
	plist += x;
	return *this;
}

Room& Room::operator-= (const Worker & x){
	wlist -= x;
	return *this;
}

Room& Room::operator-= (const Prisoner & x){
	plist -= x;
	return *this;
}

ostream& operator<<(ostream& os, Room &x){
	cout << endl <<"Room: " << x.roomID << " information:" << endl << endl;
	cout << "Privleges needed: " << x.privilegesNeeded << endl;
	cout << "Number of prisoners: "<< x.numberOfWorkers << endl;
	cout << "Number of workers: " << x.numberOfPrisoners << endl;
	cout << "Maximal level of danger: " << x.maximalDangerLvl << endl;
	cout << endl << "List of prisoners: " << endl;
	(x.plist).print();
	cout << "List of workers: " << endl;
	(x.wlist).print();
	return os;
}
