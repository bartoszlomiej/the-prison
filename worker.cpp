#include "worker.h"
#include "human.h"

using namespace std;

Worker::Worker(int id, string name, string surname, string rolea, char currentOrdera, int privileges) : Human(id, name, surname, privileges){

	cout << "New Worker added" << endl;
	role =  rolea;
	currentOrder = currentOrdera;
}

Worker::Worker(int id, string name, string surname, string rolea, char currentOrdera) : Human(id, name, surname, 0){
	role =  rolea;
	currentOrder = currentOrdera;
	cout << "New Worker added" << endl;
}

Worker::Worker(int id, string name, string surname) : Human(id, name, surname, 0){
	role = "not given\n";
	currentOrder = 'o';
}

Worker::Worker() : Human(){}

Worker::~Worker(){
	cout << "Worker of id: " << id << " removed" << endl;
}

void Worker::setRole(string s){
	role = s;
}

void Worker::setOrder(char c){
	currentOrder = c;
}

ostream& operator<< (ostream& os, Worker &x){
	x.print();	
	cout << "Role: " << x.role << endl;
	cout << "Current order: " << x.currentOrder << endl;
	return os;
}
