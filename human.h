#include <iostream>
#include <string>

using namespace std;

#ifndef HUMAN
#define HUMAN

class Human{
	protected:
		int id;
		string name;
		string surname;
		int privileges;
	public:
		Human();
		Human(int , string, string, int);
		~Human(){}
		bool checkPrivileges(int);
		void setPrivileges();
		int getID() const;
		void setData();
		void print();
};

#endif
