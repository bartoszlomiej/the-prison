#include <iostream>
#include "worker.h"
#include "prisoner.h"
#include "ulist.h"

using namespace std;

#ifndef ROOM
#define ROOM

class Room{
	private:
		int roomID;
		int privilegesNeeded;
		int numberOfWorkers;	//maximal workers in one room
		int numberOfPrisoners;	//maximal prisoners in one room
		int maximalDangerLvl;
		List<Worker> wlist;
		List<Prisoner> plist;
	public:
		Room();
		Room(int id);
		Room(int id, int privileges);
		Room(int id, int privileges, int numberOfWorkers, int numberOfPrisoners, int maximalDangerLvl);
		Room(int id, int privileges, int numberOfWorkers, int numberOfPrisoners, int maximalDangerLvl,const List<Worker> &w);
		Room(int id, int privileges, int numberOfWorkers, int numberOfPrisoners, int maximalDangerLvl,const List<Prisoner> &p);
		Room(int id, int privileges, int numberOfWorkers, int numberOfPrisoners, int maximalDangerLvl,const List<Worker> &w, const List<Prisoner> &p);
		~Room();
		Room(const Room & x);
		Room& operator= (const Room & x);	//transformes one room into another one (however the assigned people stays)
		Room& operator+= (const Worker & x); //adds worker to the room
		Room& operator+= (const Prisoner & x); //adds prisoner to the room
		Room& operator+= (const List<Worker> & x);
		Room& operator+= (const List<Prisoner> & x); 
		Room& operator-= (const Worker & x); //removes worker from the room
		Room& operator-= (const Prisoner & x); //removes prisoner from the room
		friend ostream& operator<<(ostream& os, Room &x);
};

#endif
